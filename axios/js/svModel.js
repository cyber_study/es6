function SinhVien(_ma, _ten, _email, _matKhau, _diemLy, _diemToan, _diemHoa) {
  this.ma = _ma;
  this.ten = _ten;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ly = _diemLy;
  this.toan = _diemToan;
  this.hoa = _diemHoa;

  this.tinhDTB = function () {
    var dtb = (this.diemHoa * 1 + this.diemToan * 1 + this.diemLy * 1) / 3;
    return dtb.toFixed(1);
  };
}

function requiredCheck(value, idError) {
  if (value.length == 0) {
    document.getElementById(idError).innerText = "Không được để rỗng";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function cloneCheck(idSv, listSv, idError) {
  var index = listSv.findIndex(function (sv) {
    return sv.ma == idSv;
  });

  if (index == -1) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "Mã sinh viên đã tồn tại";
    return false;
  }
}

function emailValid(value, idError) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var isEmail = re.test(value);
  if (isEmail) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "Wrong email format!";
    return false;
  }
}
