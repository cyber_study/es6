const BASE_URL = "https://633ec04483f50e9ba3b75fb4.mockapi.io/";

var loadingOn = function () {
  document.getElementById("loading").style.display = "flex";
};
var loadingOff = function () {
  document.getElementById("loading").style.display = "none";
};
// lay danh sach sinh vien
var fecthDssv = function () {
  loadingOn();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (response) {
      // console.log("error: ", response.data);
      dssv = response.data;
      renderDssv(dssv);
      loadingOff();
    })
    .catch(function (error) {
      console.log("error: ", error);
    });
};

fecthDssv();

// render danh sach sinh vien
var renderDssv = function (listSv) {
  var contentHTML = "";
  listSv.forEach(function (sv) {
    contentHTML += `
      <tr>
        <td>${sv.ma}</td>
        <td>${sv.ten}</td>
        <td>${sv.email}</td>
        <td>0</td>
        <td>
          <button onclick="suaSv('${sv.ma}')" class="btn btn-primary">Sửa</button>
          <button onclick="xoaSv('${sv.ma}')" class="btn btn-danger">Xóa</button>
        </td>
      </tr>
    `;
  });

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

// xoa sinh vien
var xoaSv = function (idSv) {
  loadingOn();
  axios({
    url: `${BASE_URL}/sv/${idSv}`,
    method: "DELETE",
  })
    .then(function (res) {
      fecthDssv();
      loadingOff();
      Swal.fire("Xóa thành công!");
      console.log("res :", res);
    })
    .catch(function (err) {
      Swal.fire("Xóa khum thành công!");
      console.log("err: ", err);
    });
  // console.log("idSv :", idSv);
};

// them sinh vien
var themSv = function () {
  var sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      fecthDssv();
      console.log("res :", res);
    })
    .catch(function (err) {
      console.log("error :", err);
    });
};

// sua thong tin sinh vien
var suaSv = function (idSv) {
  axios
    .get(`${BASE_URL}/sv/${idSv}`)
    .then(function (res) {
      document.getElementById("txtMaSV").value = res.data.ma;
      document.getElementById("txtMaSV").disabled = true;
      document.getElementById("txtTenSV").value = res.data.ten;
      document.getElementById("txtEmail").value = res.data.email;
      document.getElementById("txtPass").value = res.data.matKhau;
      document.getElementById("txtDiemLy").value = res.data.ly;
      document.getElementById("txtDiemToan").value = res.data.toan;
      document.getElementById("txtDiemHoa").value = res.data.hoa;
    })
    .catch(function (err) {
      console.log("err :", err);
    });
};

// reset form
var resetForm = function () {
  document.getElementById("formQLSV").reset();
};

// cap nhat thong tin sinh vien
var capNhat = function () {
  loadingOn();
  var sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv/${sv.ma}`,
    method: "PUT",
    data: sv,
  }).then(function () {
    // console.log(res.data);
    document.getElementById("txtMaSV").disabled = false;
    resetForm();
    loadingOff();
    fecthDssv();
    renderDssv(dssv);
    Swal.fire("Cập nhật thành công!");
  });
};
